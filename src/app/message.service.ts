import { Injectable } from '@angular/core';
import { Message } from './message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  constructor() {}

  public getMessage() {
    return Message.getMessage();
  }
}
