import { ApiClient } from './api-client';

export class Model {
  private static _api: ApiClient;
  private id: string;
  private created: number;
  private updated: number;

  static setApiClient(arg: ApiClient) {
    this._api = arg;
  }

  static api(): ApiClient {
    return this._api;
  }

  api(): ApiClient {
    return Model._api;
  }

  public getId(): string { return this.id; }
  public withId(arg: string) { this.id = arg; return this; }

  public getCreated(): number { return this.created; }
  public withCreated(arg: number) { this.created = arg; return this; }

  public getUpdated(): number { return this.updated; }
  public withUpdated(arg: number) { this.updated = arg; return this; }
}
