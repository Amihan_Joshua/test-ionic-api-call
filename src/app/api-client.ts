import { HttpClient } from '@angular/common/http';
import { MessageApi } from './message-api';

export class ApiClient {
  private readonly api: HttpClient;
  private readonly messageApi: MessageApi;

  constructor(private client: HttpClient) {
    this.api = client;
    this.messageApi = new MessageApi(this.api);
  }

  messages(): MessageApi { return this.messageApi; }
}
