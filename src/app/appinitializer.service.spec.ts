import { TestBed } from '@angular/core/testing';

import { AppinitializerService } from './appinitializer.service';

describe('AppinitializerService', () => {
  let service: AppinitializerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AppinitializerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
