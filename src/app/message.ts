import { Model } from './model';

export class Message extends Model {
  message: string;

  public static getMessage() {
    return this.api().messages().getMessage();
  }
}
