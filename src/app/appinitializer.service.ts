import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Model } from './model';
import { ApiClient } from './api-client';

@Injectable({
  providedIn: 'root'
})
export class AppInitializerService {
  constructor(private http: HttpClient) {
    this.init();
  }

  init() {
    Model.setApiClient(new ApiClient(this.http));
  }
}
