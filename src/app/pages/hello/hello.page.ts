import { Component, OnInit } from '@angular/core';
import { MessageService } from 'src/app/message.service';

@Component({
  selector: 'app-hello',
  templateUrl: './hello.page.html',
  styleUrls: ['./hello.page.scss'],
})
export class HelloPage implements OnInit {
  title;
  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.getMessage().then(response => {
      console.log(response);
      this.title = response['message'];
      console.log(this.title);
    })
  }
}
