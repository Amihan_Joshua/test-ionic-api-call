import { HttpClient } from '@angular/common/http';

export class MessageApi {
  api: HttpClient;

  constructor(api: HttpClient) {
    this.api = api;
  }

  getMessage() {
    return new Promise((resolve, reject) => {
      this.api.get(`http://localhost:8081/test`).subscribe(
        (res: any) => resolve(res),
        (err: any) => {
          console.log(err);
          reject(err);
        }
      );
    });
  }
}
